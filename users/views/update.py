from django.urls import reverse_lazy
from django.views.generic.edit import UpdateView
from users.forms.update import UpdateUserForm

from users.models import UserProfile


class UpdateUserView(UpdateView):
    form_class = UpdateUserForm
    model = UserProfile
    template_name = "users/modification/update_user.html"
    success_url = reverse_lazy('users:infos')
