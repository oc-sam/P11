from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import send_mail
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.views import View
from django.views.generic import CreateView

from users.forms.create import EmailUserCreationFrom

User = get_user_model()


class EmailSignUpView(CreateView):
    form_class = EmailUserCreationFrom
    success_url = '/'
    template_name = 'users/registration/signup.html'

    def form_valid(self, form):
        form.instance.is_active = False
        validate_form = super().form_valid(form)
        mail_subject = 'Activer votre compte Pur Beurre'
        plain_message = render_to_string(
            'users/activation/email.html',
            {
                'site': f'http://{get_current_site(self.request).domain}',
                'user': form.instance,
                'uid': urlsafe_base64_encode(force_bytes(form.instance.username)),
                'token': default_token_generator.make_token(form.instance),
            })
        send_mail(
            from_email=None,
            subject=mail_subject,
            message=plain_message,
            recipient_list=[form.instance.username],
            fail_silently=False,
        )
        messages.success(
            self.request,
            (
                'Votre compte a été créé.'
                f' Vérifier votre boite mail {form.instance.username}'
                ' pour activer votre compte.'
            )
        )
        return validate_form


class ActivationView(View):
    success_url = reverse_lazy('users:login')

    def get(self, request, **kwargs):
        """
        Check token fit to user and decode uid
        Activate user if those are OK
        """
        try:
            uid = urlsafe_base64_decode(kwargs['uidb64']).decode()
            user = User.objects.get(username=uid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if any([
            user is None,
            not default_token_generator.check_token(user, kwargs['token'])
        ]):
            messages.error(
                request,
                'Le token est incorrect. Avez-vous déjà activer votre compte ?'
            )
        else:
            user.is_active = True
            user.save()
            messages.success(
                request,
                'Votre compte a été activé. Vous pouvez vous connecter !'
            )
        return redirect(self.success_url)
