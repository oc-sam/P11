from django.test import TestCase, RequestFactory
from users.models import UserProfile
from users.backend import CustomBackend


class CustomBackendTest(TestCase):
    def setUp(self):
        self.valid_test_user = UserProfile._default_manager.create_user(
            username="test@example.com", password="test"
        )
        self.request = RequestFactory().get('/accounts/login/')

    def test_authenticate(self):
        authenticated_user1 = CustomBackend().authenticate(
            request=self.request,
            username="test@example.com",
            password="test"
        )
        self.assertEqual(self.valid_test_user, authenticated_user1)
        authenticated_user2 = CustomBackend().authenticate(
            request=self.request,
            username="test@example.com",
            password="wrong_password"
        )
        self.assertIsNone(authenticated_user2)
        authenticated_user3 = CustomBackend().authenticate(
            request=self.request,
            username="user@doesnotexist.com",
            password="wrong_password"
        )
        self.assertIsNone(authenticated_user3)

    def test_get_user(self):
        got_user = CustomBackend().get_user(self.valid_test_user.pk)
        self.assertEqual(self.valid_test_user, got_user)

        invalid_pk = got_user.pk
        got_user.delete()
        self.assertIsNone(CustomBackend().get_user(invalid_pk))
