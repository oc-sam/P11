from django.forms.models import model_to_dict
from django.test import TestCase

from users.models import UserProfile


class BookUpdateTest(TestCase):
    fixtures = ['data.json']
    def test_update_book(self):
        user = UserProfile.objects.last()
        new_infos = {
                'username': 'test@username.com',
                'last_name': 'lastname',
                'first_name': 'firstname'
            }

        response = self.client.post(
            f"/accounts/edit/{user.pk}/", 
            data=new_infos
        )

        self.assertEqual(response.status_code, 302)

        user.refresh_from_db()
        self.assertDictEqual(
            new_infos,
            model_to_dict(user, ['username', 'last_name', 'first_name'])
        )