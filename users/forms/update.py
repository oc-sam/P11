from django import forms

from users.models import UserProfile


class UpdateUserForm(forms.ModelForm):
    username = forms.EmailField(
        required=False,
        help_text="Veuillez saisir une adresse email valide.",
        widget=forms.TextInput(attrs={
            "autofocus": True,
            "class": "d-flex"}
        ),
        label="Email"
    )
    first_name = forms.CharField(
        required=False,
        help_text="Veuillez saisir un prénom valide.",
        widget=forms.TextInput(attrs={
            "autofocus": True,
            "class": "d-flex"}
        ),
        label="Prénom"
    )
    last_name = forms.CharField(
        required=False,
        help_text="Veuillez saisir un nom de famille valide.",
        widget=forms.TextInput(attrs={
            "autofocus": True,
            "class": "d-flex"}
        ),
        label="Nom de famille"
    )

    class Meta:
        model = UserProfile
        fields = ("username", "first_name", "last_name")
