from django.contrib.auth.backends import BaseBackend

from users.models import UserProfile


class CustomBackend(BaseBackend):

    def authenticate(self, request, username=None, password=None):
        try:
            user = UserProfile.objects.get(username__iexact=username)
            return user if user.check_password(password) else None
        except UserProfile.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return UserProfile.objects.get(pk=user_id)
        except UserProfile.DoesNotExist:
            return None
