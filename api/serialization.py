import contextlib
from itertools import starmap

from django.apps import apps
from django.core.exceptions import ValidationError

from api.representation import OpenFoodApi


class OpenDeserializer(OpenFoodApi):

    def __init__(self, api_res):
        self.app_name = 'substitute'
        self.ProductModel, self.CategoryModel, self.StoreModel = tuple(
            starmap(
                apps.get_model,
                (
                    (self.app_name, 'Product'),
                    (self.app_name, 'Category'),
                    (self.app_name, 'Store')
                )
            )
        )
        self.api_res = api_res

    def get_openfood_data(self):
        """
        Deserialize openfoodfacts cleaned data into model instances.

        Returns:
            tuple: Cleaned data from OFFAPI, unsaved products, categories an stores.
        """
        p_unsaved, cats, stores = [], [], []
        for prods in self.api_res:
            for prod in prods:
                self.build_unsaved_prods(p_unsaved, prod)
                cats += prod['categories']
                stores += prod['stores_tags']
        c_unsaved, s_unsaved = tuple(
            starmap(
                self.build_unsaved_inst,
                ((cats, self.CategoryModel), (stores, self.StoreModel))
            )
        )

        return p_unsaved, c_unsaved, s_unsaved

    def build_unsaved_prods(self, p_unsaved, prod):
        """
        Create Product instances from OFFAPI.

        Args:
            p_unsaved (list): unsaved product instances.
            prod (dict): cleaned product data from OFFAPI.
        """
        with contextlib.suppress(KeyError):
            prod_unsaved_inst = self.ProductModel(
                name=prod['generic_name_fr'],
                url=prod['url'],
                nutriscore=prod['nutriscore_grade'],
                barcode=prod['code'],
                image_url=prod['image_front_url'],
                image_nutri=prod['image_nutrition_url']
            )
            if not self.catch_unvalid(prod_unsaved_inst):
                p_unsaved.append((prod_unsaved_inst))

    def build_unsaved_inst(self, related_data, a_model):
        """
        Create Category or Store instances from OFFAPI.

        Args:
            related_data (list): cleaned data from OFFAPI.
            a_model (Model): Model to instanciate.

        Returns:
            list: unsaved instances of model.
        """
        return [
            inst for inst in
            [a_model(name=inst_name) for inst_name in set(related_data)]
            if not self.catch_unvalid(inst)
        ]

    @staticmethod
    def catch_unvalid(inst):
        """
        catch_unvalid calls full_clean model method
        on model instances to validate its data.

        Args:
            inst : model instance

        Returns:
            bool: is it clean enough ?
        """
        try:
            inst.full_clean()
            return False
        except ValidationError:
            return True
