CATEGORIES = {
    "wanted_data": ("name"),
    "url": "https://fr.openfoodfacts.org/categories",
    "params": {
        "limit": 5,  # nbr of wanted cat + 1
        "skip": "known",
        "json": True,
        "sort_by": "products",  # get categories with max products
    },
}

PROD_IN_CAT = {
    "wanted_data": (
        "generic_name_fr",
        "categories",
        "nutriscore_grade",
        "stores_tags",
        "url",
        'brands_tags',
        'code',
        'image_front_url',
        'image_nutrition_url'
    ),
    "url": "https://fr.openfoodfacts.org/cgi/search.pl",
    "params": {
        "action": "process",
        "tagtype_0": "categories",
        "tag_contains_0": "contains",
        "tag_0": "Plant-based foods and beverages",
        "tagtype_1": 'categories_lc',
        "tag_contains_1": "contains",
        "tag_1": "fr",
        "json": True,
        "page_size": 20
    },
}
