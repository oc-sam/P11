PYTHON_INTERPRETER=python3.9
VENV_PATH=.venv

PYTHON_BIN=$(VENV_PATH)/bin/python
PIP=$(VENV_PATH)/bin/pip
DJANGO_MANAGE=$(VENV_PATH)/bin/python manage.py
FLAKE=$(VENV_PATH)/bin/flake8

# Formatting variables, FORMATRESET is always to be used last to close formatting
FORMATBLUE:=$(shell tput setab 4)
FORMATBOLD:=$(shell tput bold)
FORMATRESET:=$(shell tput sgr0)

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo
	@echo "  clean                         -- to clean EVERYTHING (Warning)"
	@echo "  clean-pycache                 -- to remove all __pycache__, this is recursive from current directory"
	@echo "  clean-backend-install         -- to clean backend installation"
	@echo
	@echo "  install                       -- to install backend"
	@echo
	@echo "  superuser                     -- to create a superuser for Django admin"
	@echo
	@echo "  run                           -- to run backend development server"
	@echo "  migrate                       -- to apply backend model migrations"
	@echo "  migrations                    -- to generate backend model migrations"
	@echo "  check-migrations              -- to check for pending backend model migrations (do not write anything)"
	@echo
	@echo "  flake                         -- to launch Flake8 checking"
	@echo "  tests                         -- to launch tests"
	@echo "  coverage                      -- to generate coverage report"
	@echo "  quality                       -- to launch all quality tasks (flake check-migrations coverage)"
	@echo

clean-pycache:
	@echo ""
	@printf "$(FORMATBLUE)$(FORMATBOLD)---> Cleaning Python cache files <---$(FORMATRESET)\n"
	@echo ""
	rm -Rf .pytest_cache
	find . -type d -name "__pycache__"|xargs rm -Rf
	find . -name "*\.pyc"|xargs rm -f
.PHONY: clean-pycache

clean-backend-install:
	@echo ""
	@printf "$(FORMATBLUE)$(FORMATBOLD)---> Cleaning backend install <---$(FORMATRESET)\n"
	@echo ""
	rm -Rf $(VENV_PATH)
.PHONY: clean-backend-install

clean: clean-backend-install clean-pycache
.PHONY: clean

venv:
	@echo ""
	@printf "$(FORMATBLUE)$(FORMATBOLD)---> Initializing Python virtual environment <---$(FORMATRESET)\n"
	@echo ""
	virtualenv -p $(PYTHON_INTERPRETER) $(VENV_PATH)
.PHONY: venv

migrate:
	@echo ""
	@printf "$(FORMATBLUE)$(FORMATBOLD)---> Applying backend models migrations <---$(FORMATRESET)\n"
	@echo ""
	$(DJANGO_MANAGE) migrate
.PHONY: migrate

migrations:
	@echo ""
	@printf "$(FORMATBLUE)$(FORMATBOLD)---> Generate backend models migrations <---$(FORMATRESET)\n"
	@echo ""
	$(DJANGO_MANAGE) makemigrations
.PHONY: migrations

install-backend:
	@echo ""
	@printf "$(FORMATBLUE)$(FORMATBOLD)---> Installing backend requirements <---$(FORMATRESET)\n"
	@echo ""
	$(PIP) install -r requirements.txt
.PHONY: install-backend

install: venv install-backend migrate
.PHONY: install

check-migrations:
	@echo ""
	@printf "$(FORMATBLUE)$(FORMATBOLD)---> Checking for pending backend model migrations <---$(FORMATRESET)\n"
	@echo ""
	$(DJANGO_MANAGE) makemigrations --check --dry-run -v 3
.PHONY: check-migrations

superuser:
	@echo ""
	@printf "$(FORMATBLUE)$(FORMATBOLD)---> Creating a super user <---$(FORMATRESET)\n"
	@echo ""
	$(DJANGO_MANAGE) createsuperuser
.PHONY: superuser

run:
	@echo ""
	@printf "$(FORMATBLUE)$(FORMATBOLD)---> Running backend development server <---$(FORMATRESET)\n"
	@echo ""
	$(DJANGO_MANAGE) runserver -v 3
.PHONY: run


flake:
	@echo ""
	@printf "$(FORMATBLUE)$(FORMATBOLD)---> Running backend code style validation <---$(FORMATRESET)\n"
	@echo ""
	$(FLAKE) --statistics --show-source
.PHONY: flake

tests:
	@echo ""
	@printf "$(FORMATBLUE)$(FORMATBOLD)---> Running backend tests <---$(FORMATRESET)\n"
	@echo ""
	$(DJANGO_MANAGE) test --timing --pdb --failfast -v 2
.PHONY: tests


quality: flake check-migrations coverage
	@echo ""
	@echo "♥ ♥ Everything should be fine ♥ ♥"
	@echo ""
.PHONY: quality

coverage:
	@echo ""
	@printf "$(FORMATBLUE)$(FORMATBOLD)---> Generating coverage report <---$(FORMATRESET)\n"
	@echo ""
	coverage run --source='.' manage.py test -v 2 && coverage report -m --fail-under=80
.PHONY: coverage