import factory
from substitute.models.store import Store


class StoreFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('company')

    @factory.post_generation
    def products(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for product in extracted:
                self.products.add(product)

    class Meta:
        model = Store
