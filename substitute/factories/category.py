import factory
from substitute.models.category import Category


class CategoryFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('word')

    @factory.post_generation
    def products(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for product in extracted:
                self.products.add(product)

    class Meta:
        model = Category
