import factory
from substitute.models.product import Product


class ProductFactory(factory.django.DjangoModelFactory):

    name = factory.Faker('word')
    url = factory.Faker("url")
    nutriscore = factory.Iterator(['a', 'b', 'c', 'd', 'e', 'f'])
    barcode = factory.Faker('ean')
    image_url = factory.Faker('image_url')
    image_nutri = factory.Faker('image_url')

    class Meta:
        model = Product
