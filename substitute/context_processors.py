from substitute.forms.search import SearchForm


def search_navbar(request):
    return {"searchform": SearchForm()}
