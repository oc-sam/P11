from django.test import TestCase

from users.models import UserProfile


class ModelAdminPermissionTests(TestCase):
    def setUp(self):
        superuser = UserProfile.objects.create_superuser(
            username='admin@example.com', password='secret', email='admin@example.com'
        )
        self.client.force_login(superuser)


    def test_has_add_permission_always_false(self):
        self.response = self.client.get('/admin/substitute/favorite/add/')
        assert self.response.status_code == 403
