def get_msg(prod, procedure, outcome):
    if procedure == "delete":
        if outcome == "success":
            return (
                "Le produit suivant a bien été supprimé de vos favoris:\n"
                f"{prod}"
            )
        elif outcome == "fail":
            return (
                "Le produit suivant n'a pas été supprimé de vos favoris:\n"
                f"{prod}"
            )
    elif procedure == "add":
        if outcome == "success":
            return (
                "Le produit suivant a bien été enregistré dans vos favoris:\n"
                f"{prod}"
            )
        elif outcome == "fail":
            return (
                "Le produit suivant n'a pas été enregistré dans vos favoris:\n"
                f"{prod}"
            )
    else:
        raise AttributeError("You're not asking for the right message !")
