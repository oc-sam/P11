from django.core.management.base import BaseCommand
from substitute.factories import CategoryFactory, FavoriteFactory, StoreFactory


class Command(BaseCommand):
    help = 'Create instance for Category, Favorite, Product and Store models.'

    def add_arguments(self, parser):
        parser.add_argument('nbr_favorites', nargs='+', type=int)

    def handle(self, *args, **options):
        favorites = [FavoriteFactory() for _ in options['nbr_favorites']]
        fav_products = []
        fav_substitutes = []
        for favorite in favorites:
            self.stdout.write(
                f"Favorite created: {favorite.user} / "
                f"{favorite.product} / {favorite.subsitute}.\n"
            )
            fav_products.append(favorite.product)
            fav_substitutes.append(favorite.subsitute)
        store1 = StoreFactory.create(products=fav_products)
        cat1 = CategoryFactory.create(products=fav_products)
        store2 = StoreFactory.create(products=fav_substitutes)
        cat2 = CategoryFactory.create(products=fav_substitutes)
        for elt in (store1, cat1, store2, cat2):
            self.stdout.write(
                f"{elt._meta.model.__name__} created: {elt.name}.\n"
            )
