from django import forms

from substitute.models.favorite import Favorite
from substitute.models.product import Product


class FavForm(forms.ModelForm):
    product = forms.ModelChoiceField(queryset=Product.objects.all())

    class Meta:
        model = Favorite
        fields = ["product"]
