from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=255, unique=True)
    url = models.URLField(blank=False)
    nutriscore = models.CharField(max_length=1)
    barcode = models.CharField(max_length=255, unique=True)
    image_url = models.URLField(blank=False)
    image_nutri = models.URLField(blank=False)

    def __str__(self):
        return f"{self.name}"
