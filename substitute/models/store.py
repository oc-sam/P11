from django.db import models


class Store(models.Model):
    name = models.CharField(max_length=255, unique=True)
    products = models.ManyToManyField("Product",
                                      related_name="stores",
                                      blank=True)

    def __str__(self):
        return f"{self.name}"
