from django.db import models


class Favorite(models.Model):
    user = models.ForeignKey("users.UserProfile",
                             related_name=("favorites"),
                             on_delete=models.CASCADE)
    product = models.ForeignKey("Product",
                                related_name=("favorites"),
                                on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.user.username}=>{self.product.name}"
