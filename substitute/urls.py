from django.contrib.auth.decorators import login_required
from django.urls import path

from substitute.views.favorite import (AddFavoriteView, FavoritesView,
                                       RemoveFavoriteView)
from substitute.views.home import HomeView
from substitute.views.product import ProductDetailView, SearchResultView

app_name = 'substitute'

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('substitute', SearchResultView.as_view(), name='search'),
    path('product/<int:pk>', ProductDetailView.as_view(), name='prod-detail'),
    path('favorites/', login_required(FavoritesView.as_view()), name='all_favorites'),
    path(
        'remove_favorite/<int:pk>',
        login_required(RemoveFavoriteView.as_view()),
        name='remove_favorite'
    ),
    path(
        'add_favorite/<int:pk>',
        login_required(AddFavoriteView.as_view()),
        name='add_favorite'
    ),
]
